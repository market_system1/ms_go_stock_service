package postgres

import (
	"context"
	"fmt"
	"log"
	"market_system/ms_go_stock_service/config"
	"market_system/ms_go_stock_service/storage"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db              *pgxpool.Pool
	coming          storage.ComingRepoI
	arrival_content storage.ArrivalContentRepoI
	remainder       storage.RemainderRepoI
}

type Pool struct {
	db *pgxpool.Pool
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}

func (s *Store) Coming() storage.ComingRepoI {
	if s.coming == nil {
		s.coming = NewComingRepo(s.db)
	}

	return s.coming
}

func (s *Store) ArrivalContent() storage.ArrivalContentRepoI {
	if s.arrival_content == nil {
		s.arrival_content = NewArrivalContentRepo(s.db)
	}

	return s.arrival_content
}

func (s *Store) Remainder() storage.RemainderRepoI {
	if s.remainder == nil {
		s.remainder = NewRemainderRepo(s.db)
	}

	return s.remainder
}
