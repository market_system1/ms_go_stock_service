package postgres

import (
	"context"
	"database/sql"
	"market_system/ms_go_stock_service/genproto/stock_service"
	"market_system/ms_go_stock_service/pkg/helper"
	"market_system/ms_go_stock_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ArrivalContentRepo struct {
	db *pgxpool.Pool
}

func NewArrivalContentRepo(db *pgxpool.Pool) storage.ArrivalContentRepoI {
	return &ArrivalContentRepo{
		db: db,
	}
}

func (c *ArrivalContentRepo) Create(ctx context.Context, req *stock_service.CreateArrivalContent) (resp *stock_service.ArrivalContentPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "arrival_content" (
				id,
				coming_id,
				brand_id,
				category_id,
				name,
				barcode,
				quantity,
				price_arrival,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetComingId(),
		req.GetBrandId(),
		req.GetCategoryId(),
		req.GetName(),
		req.GetBarcode(),
		req.GetQuantity(),
		req.GetPriceArrival(),
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.ArrivalContentPrimaryKey{Id: id.String()}, nil
}

func (c *ArrivalContentRepo) GetByPKey(ctx context.Context, req *stock_service.ArrivalContentPrimaryKey) (resp *stock_service.ArrivalContent, err error) {

	query := `
		SELECT
			id,
			coming_id,
			brand_id,
			category_id,
			name,
			barcode,
			quantity,
			price_arrival,
			created_at,
			updated_at
		FROM "arrival_content"
		WHERE id = $1
	`

	var (
		id            sql.NullString
		coming_id     sql.NullString
		brand_id      sql.NullString
		category_id   sql.NullString
		name          sql.NullString
		barcode       sql.NullString
		quantity      sql.NullInt64
		price_arrival sql.NullFloat64
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&coming_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&quantity,
		&price_arrival,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.ArrivalContent{
		Id:           id.String,
		ComingId:     coming_id.String,
		BrandId:      brand_id.String,
		CategoryId:   category_id.String,
		Name:         name.String,
		Barcode:      barcode.String,
		Quantity:     quantity.Int64,
		PriceArrival: price_arrival.Float64,
		CreatedAt:    createdAt.String,
		UpdatedAt:    updatedAt.String,
	}

	return
}

func (c *ArrivalContentRepo) GetAll(ctx context.Context, req *stock_service.GetListArrivalContentRequest) (resp *stock_service.GetListArrivalContentResponse, err error) {

	resp = &stock_service.GetListArrivalContentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_id,
			brand_id,
			category_id,
			name,
			barcode,
			quantity,
			price_arrival,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "arrival_content"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetComingId()) > 0 {
		filter += " AND coming_id ILIKE '%" + req.GetComingId() + "%'"
	}

	if len(req.GetBrandId()) > 0 {
		filter += " AND brand_id ILIKE '%" + req.GetBrandId() + "%'"
	}

	if len(req.GetCategoryId()) > 0 {
		filter += " AND category_id ILIKE '%" + req.GetCategoryId() + "%'"
	}

	if len(req.GetBarcode()) > 0 {
		filter += " AND barcode ILIKE '%" + req.GetBarcode() + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id            sql.NullString
			coming_id     sql.NullString
			brand_id      sql.NullString
			category_id   sql.NullString
			name          sql.NullString
			barcode       sql.NullString
			quantity      sql.NullInt64
			price_arrival sql.NullFloat64
			createdAt     sql.NullString
			updatedAt     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&coming_id,
			&brand_id,
			&category_id,
			&name,
			&barcode,
			&quantity,
			&price_arrival,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.ArrivalContents = append(resp.ArrivalContents, &stock_service.ArrivalContent{
			Id:           id.String,
			ComingId:     coming_id.String,
			BrandId:      brand_id.String,
			CategoryId:   category_id.String,
			Name:         name.String,
			Barcode:      barcode.String,
			Quantity:     quantity.Int64,
			PriceArrival: price_arrival.Float64,
			CreatedAt:    createdAt.String,
			UpdatedAt:    updatedAt.String,
		})
	}

	return
}

func (c *ArrivalContentRepo) Update(ctx context.Context, req *stock_service.UpdateArrivalContent) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "arrival_content"
			SET
					coming_id = :coming_id,
					brand_id = :brand_id,
					category_id = :category_id,
					name = :name,
					barcode = :barcode,
					quantity = :quantity,
					price_arrival = :price_arrival,
					updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":            req.GetId(),
		"coming_id":     req.GetComingId(),
		"brand_id":      req.GetBrandId(),
		"category_id":   req.GetCategoryId(),
		"name":          req.GetName(),
		"barcode":       req.GetBarcode(),
		"quantity":      req.GetQuantity(),
		"price_arrival": req.GetPriceArrival(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ArrivalContentRepo) Delete(ctx context.Context, req *stock_service.ArrivalContentPrimaryKey) error {

	query := `DELETE FROM "arrival_content" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
