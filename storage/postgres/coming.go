package postgres

import (
	"context"
	"database/sql"
	"market_system/ms_go_stock_service/genproto/stock_service"
	"market_system/ms_go_stock_service/pkg/helper"
	"market_system/ms_go_stock_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ComingRepo struct {
	db *pgxpool.Pool
}

func NewComingRepo(db *pgxpool.Pool) storage.ComingRepoI {
	return &ComingRepo{
		db: db,
	}
}

func (c *ComingRepo) Create(ctx context.Context, req *stock_service.CreateComing) (resp *stock_service.ComingPrimaryKey, err error) {

	var (
		id = uuid.New()
	)
	cominIncrementId, err := helper.NewIncrementId(c.db, "coming_id", "coming", "p", 7)
	if err != nil {
		return nil, err
	}
	query := `INSERT INTO "coming" (
				id,
				coming_id,
				branch_id,
				provider_id,
				updated_at
			) VALUES ($1, $2, $3, $4, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		cominIncrementId(),
		req.GetBranchId(),
		req.GetProviderId(),
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.ComingPrimaryKey{Id: id.String()}, nil
}

func (c *ComingRepo) GetByPKey(ctx context.Context, req *stock_service.ComingPrimaryKey) (resp *stock_service.Coming, err error) {

	query := `
		SELECT
			id,
			coming_id,
			branch_id,
			provider_id,
			date_time,
			status,
			created_at,
			updated_at
		FROM "coming"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		coming_id   sql.NullString
		branch_id   sql.NullString
		provider_id sql.NullString
		date_time   sql.NullString
		status      sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&coming_id,
		&branch_id,
		&provider_id,
		&date_time,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.Coming{
		Id:         id.String,
		ComingId:   coming_id.String,
		BranchId:   branch_id.String,
		ProviderId: provider_id.String,
		DateTime:   date_time.String,
		Status:     status.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *ComingRepo) GetAll(ctx context.Context, req *stock_service.GetListComingRequest) (resp *stock_service.GetListComingResponse, err error) {

	resp = &stock_service.GetListComingResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_id,
			branch_id,
			provider_id,
			date_time,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "coming"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetComingId()) > 0 {
		filter += " AND coming_id ILIKE '%" + req.GetComingId() + "%'"
	}

	if len(req.GetBranchId()) > 0 {
		filter += " AND branch_id ILIKE '%" + req.GetBranchId() + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			coming_id   sql.NullString
			branch_id   sql.NullString
			provider_id sql.NullString
			date_time   sql.NullString
			status      sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&coming_id,
			&branch_id,
			&provider_id,
			&date_time,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Comings = append(resp.Comings, &stock_service.Coming{
			Id:         id.String,
			ComingId:   coming_id.String,
			BranchId:   branch_id.String,
			ProviderId: provider_id.String,
			DateTime:   date_time.String,
			Status:     status.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *ComingRepo) Update(ctx context.Context, req *stock_service.UpdateComing) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "coming"
			SET
				coming_id = :coming_id,
				branch_id = :branch_id,
				provider_id = :provider_id,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"coming_id":   req.GetComingId(),
		"branch_id":   req.GetBranchId(),
		"provider_id": req.GetProviderId(),
		"status":      req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ComingRepo) Delete(ctx context.Context, req *stock_service.ComingPrimaryKey) error {

	query := `DELETE FROM "coming" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
