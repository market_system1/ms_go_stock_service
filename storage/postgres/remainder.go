package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"market_system/ms_go_stock_service/genproto/stock_service"
	"market_system/ms_go_stock_service/pkg/helper"
	"market_system/ms_go_stock_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type RemainderRepo struct {
	db *pgxpool.Pool
}

func NewRemainderRepo(db *pgxpool.Pool) storage.RemainderRepoI {
	return &RemainderRepo{
		db: db,
	}
}

func (c *RemainderRepo) Create(ctx context.Context, req *stock_service.CreateRemainder) (resp *stock_service.RemainderPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "remainder" (
				id,
				branch_id,
				brand_id,
				category_id,
				name,
				barcode,
				price_arrival,
				quantity,
				total,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetBranchId(),
		req.GetBrandId(),
		req.GetCategoryId(),
		req.GetName(),
		req.GetBarcode(),
		req.GetPriceArrival(),
		req.GetQuantity(),
		req.GetTotal(),
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.RemainderPrimaryKey{Id: id.String()}, nil
}

func (c *RemainderRepo) GetByPKey(ctx context.Context, req *stock_service.RemainderPrimaryKey) (resp *stock_service.Remainder, err error) {

	query := `
		SELECT
			id,
			branch_id,
			brand_id,
			category_id,
			name,
			barcode,
			price_arrival,
			quantity,
			total,
			created_at,
			updated_at
		FROM "remainder"
		WHERE id = $1
	`

	var (
		id            sql.NullString
		branch_id     sql.NullString
		brand_id      sql.NullString
		category_id   sql.NullString
		name          sql.NullString
		barcode       sql.NullString
		price_arrival sql.NullFloat64
		quantity      sql.NullInt64
		total         sql.NullFloat64
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&price_arrival,
		&quantity,
		&total,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.Remainder{
		Id:           id.String,
		BranchId:     branch_id.String,
		BrandId:      brand_id.String,
		CategoryId:   category_id.String,
		Name:         name.String,
		Barcode:      barcode.String,
		PriceArrival: price_arrival.Float64,
		Quantity:     quantity.Int64,
		Total:        total.Float64,
		CreatedAt:    createdAt.String,
		UpdatedAt:    updatedAt.String,
	}

	return
}

func (c *RemainderRepo) GetByBranchKey(ctx context.Context, req *stock_service.RemainderBarcodeBranchIdKey) (resp *stock_service.Remainder, err error) {

	query := `
		SELECT
			id,
			branch_id,
			brand_id,
			category_id,
			name,
			barcode,
			price_arrival,
			quantity,
			total,
			created_at,
			updated_at
		FROM "remainder"
		WHERE branch_id = $1 AND barcode = $2
	`

	var (
		id            sql.NullString
		branch_id     sql.NullString
		brand_id      sql.NullString
		category_id   sql.NullString
		name          sql.NullString
		barcode       sql.NullString
		price_arrival sql.NullFloat64
		quantity      sql.NullInt64
		total         sql.NullFloat64
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.BranchId, req.Barcode).Scan(
		&id,
		&branch_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&price_arrival,
		&quantity,
		&total,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.Remainder{
		Id:           id.String,
		BranchId:     branch_id.String,
		BrandId:      brand_id.String,
		CategoryId:   category_id.String,
		Name:         name.String,
		Barcode:      barcode.String,
		PriceArrival: price_arrival.Float64,
		Quantity:     quantity.Int64,
		Total:        total.Float64,
		CreatedAt:    createdAt.String,
		UpdatedAt:    updatedAt.String,
	}

	return
}

func (c *RemainderRepo) GetAll(ctx context.Context, req *stock_service.GetListRemainderRequest) (resp *stock_service.GetListRemainderResponse, err error) {

	resp = &stock_service.GetListRemainderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			brand_id,
			category_id,
			name,
			barcode,
			price_arrival,
			quantity,
			total,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "remainder"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetBranchId()) > 0 {
		filter += " AND branch_id ILIKE '%" + req.GetBranchId() + "%'"
	}
	if len(req.GetBrandId()) > 0 {
		filter += " AND brand_id ILIKE '%" + req.GetBrandId() + "%'"
	}

	if len(req.GetCategoryId()) > 0 {
		filter += " AND category_id ILIKE '%" + req.GetCategoryId() + "%'"
	}

	if len(req.GetBarcode()) > 0 {
		filter += " AND barcode ILIKE '%" + req.GetBarcode() + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id            sql.NullString
			branch_id     sql.NullString
			brand_id      sql.NullString
			category_id   sql.NullString
			name          sql.NullString
			barcode       sql.NullString
			price_arrival sql.NullFloat64
			quantity      sql.NullInt64
			total         sql.NullFloat64
			createdAt     sql.NullString
			updatedAt     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&brand_id,
			&category_id,
			&name,
			&barcode,
			&price_arrival,
			&quantity,
			&total,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Remainders = append(resp.Remainders, &stock_service.Remainder{
			Id:           id.String,
			BranchId:     branch_id.String,
			BrandId:      brand_id.String,
			CategoryId:   category_id.String,
			Name:         name.String,
			Barcode:      barcode.String,
			PriceArrival: price_arrival.Float64,
			Quantity:     quantity.Int64,
			Total:        total.Float64,
			CreatedAt:    createdAt.String,
			UpdatedAt:    updatedAt.String,
		})
	}

	return
}

func (c *RemainderRepo) Update(ctx context.Context, req *stock_service.UpdateRemainder) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "remainder"
			SET
				branch_id = :branch_id,
				brand_id = :brand_id,
				category_id = :category_id,
				name = :name,
				barcode = :barcode,
				price_arrival = :price_arrival,
				quantity = :quantity,
				total = :total,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":            req.GetId(),
		"branch_id":     req.GetBranchId(),
		"brand_id":      req.GetBrandId(),
		"category_id":   req.GetCategoryId(),
		"name":          req.GetName(),
		"barcode":       req.GetBarcode(),
		"price_arrival": req.GetPriceArrival(),
		"quantity":      req.GetQuantity(),
		"total":         req.GetTotal(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *RemainderRepo) Delete(ctx context.Context, req *stock_service.RemainderPrimaryKey) error {

	query := `DELETE FROM "remainder" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}

func (c *RemainderRepo) GetBarcode(ctx context.Context, req *stock_service.GetBarcodeReq) (resp *stock_service.Remainder, err error) {
	query := `
		SELECT
			id,
			branch_id,
			brand_id,
			category_id,
			name,
			barcode,
			price_arrival,
			quantity,
			total,
			created_at,
			updated_at
		FROM "remainder"
		WHERE barcode = $1
	`
	var (
		id            sql.NullString
		branch_id     sql.NullString
		brand_id      sql.NullString
		category_id   sql.NullString
		name          sql.NullString
		barcode       sql.NullString
		price_arrival sql.NullFloat64
		quantity      sql.NullInt64
		total         sql.NullFloat64
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)
	fmt.Println(req.Barcode)
	err = c.db.QueryRow(ctx, query, req.Barcode).Scan(
		&id,
		&branch_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&price_arrival,
		&quantity,
		&total,
		&createdAt,
		&updatedAt,
	)
	if err != nil {
		return resp, err
	}

	resp = &stock_service.Remainder{
		Id:           id.String,
		BranchId:     branch_id.String,
		BrandId:      brand_id.String,
		CategoryId:   category_id.String,
		Name:         name.String,
		Barcode:      barcode.String,
		PriceArrival: price_arrival.Float64,
		Quantity:     quantity.Int64,
		Total:        total.Float64,
		CreatedAt:    createdAt.String,
		UpdatedAt:    updatedAt.String,
	}
	return
}
