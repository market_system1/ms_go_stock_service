package storage

import (
	"context"
	"market_system/ms_go_stock_service/genproto/stock_service"
)

type StorageI interface {
	CloseDB()
	ArrivalContent() ArrivalContentRepoI
	Coming() ComingRepoI
	Remainder() RemainderRepoI
}

type ArrivalContentRepoI interface {
	Create(ctx context.Context, req *stock_service.CreateArrivalContent) (resp *stock_service.ArrivalContentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *stock_service.ArrivalContentPrimaryKey) (resp *stock_service.ArrivalContent, err error)
	GetAll(ctx context.Context, req *stock_service.GetListArrivalContentRequest) (resp *stock_service.GetListArrivalContentResponse, err error)
	Update(ctx context.Context, req *stock_service.UpdateArrivalContent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *stock_service.ArrivalContentPrimaryKey) error
}

type ComingRepoI interface {
	Create(ctx context.Context, req *stock_service.CreateComing) (resp *stock_service.ComingPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *stock_service.ComingPrimaryKey) (resp *stock_service.Coming, err error)
	GetAll(ctx context.Context, req *stock_service.GetListComingRequest) (resp *stock_service.GetListComingResponse, err error)
	Update(ctx context.Context, req *stock_service.UpdateComing) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *stock_service.ComingPrimaryKey) error
}

type RemainderRepoI interface {
	Create(ctx context.Context, req *stock_service.CreateRemainder) (resp *stock_service.RemainderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *stock_service.RemainderPrimaryKey) (resp *stock_service.Remainder, err error)
	GetByBranchKey(ctx context.Context, req *stock_service.RemainderBarcodeBranchIdKey) (resp *stock_service.Remainder, err error)
	GetAll(ctx context.Context, req *stock_service.GetListRemainderRequest) (resp *stock_service.GetListRemainderResponse, err error)
	Update(ctx context.Context, req *stock_service.UpdateRemainder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *stock_service.RemainderPrimaryKey) error
	GetBarcode(ctx context.Context, req *stock_service.GetBarcodeReq) (resp *stock_service.Remainder, err error)
}
