package service

import (
	"context"
	"market_system/ms_go_stock_service/config"
	"market_system/ms_go_stock_service/genproto/stock_service"
	"market_system/ms_go_stock_service/grpc/client"
	"market_system/ms_go_stock_service/pkg/logger"
	"market_system/ms_go_stock_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ArrivalContentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*stock_service.UnimplementedArrivalContentServiceServer
}

func NewArrivalContentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ArrivalContentService {
	return &ArrivalContentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

// func (i *ArrivalContentService) Create(ctx context.Context, req *stock_service.CreateArrivalContent) (resp *stock_service.ArrivalContent, err error) {

// 	i.log.Info("---CreateArrivalContent------>", logger.Any("req", req))

// 	pKey, err := i.strg.ArrivalContent().Create(ctx, req)
// 	if err != nil {
// 		i.log.Error("!!!CreateArrivalContent->ArrivalContent->Create--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	resp, err = i.strg.ArrivalContent().GetByPKey(ctx, pKey)
// 	if err != nil {
// 		i.log.Error("!!!GetByPKeyArrivalContent->ArrivalContent->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return
// }

func (i *ArrivalContentService) Create(ctx context.Context, req *stock_service.CreateArrivalContent) (resp *stock_service.ArrivalContent, err error) {

	i.log.Info("---CreateArrivalContent------>", logger.Any("req", req))
	pKey, err := i.strg.ArrivalContent().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateArrivalContent->ArrivalContent->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp, err = i.strg.ArrivalContent().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyArrivalContent->ArrivalContent->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	barcode := stock_service.GetBarcodeReq{
		Barcode: req.Barcode,
	}
	remainder, err := i.strg.Remainder().GetBarcode(ctx, &barcode)

	if err != nil {
		i.log.Error("!!!GetBarcodeArrivalContent->ArrivalContent->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if remainder != nil {
		_, err = i.strg.Remainder().Update(ctx, &stock_service.UpdateRemainder{
			Id:           remainder.Id,
			BranchId:     remainder.BranchId,
			BrandId:      req.BrandId,
			CategoryId:   remainder.CategoryId,
			Name:         req.Name,
			Barcode:      remainder.Barcode,
			PriceArrival: req.PriceArrival,
			Quantity:     req.Quantity + remainder.Quantity,
			Total:        req.PriceArrival * (float64(remainder.Quantity) + float64(req.Quantity)),
		})

		if err != nil {
			i.log.Error("!!!UpdateRemainContent--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	}
	return
}

func (i *ArrivalContentService) GetByID(ctx context.Context, req *stock_service.ArrivalContentPrimaryKey) (resp *stock_service.ArrivalContent, err error) {

	i.log.Info("---GetArrivalContentByID------>", logger.Any("req", req))

	resp, err = i.strg.ArrivalContent().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetArrivalContentByID->ArrivalContent->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ArrivalContentService) GetList(ctx context.Context, req *stock_service.GetListArrivalContentRequest) (resp *stock_service.GetListArrivalContentResponse, err error) {

	i.log.Info("---GetArrivalContents------>", logger.Any("req", req))

	resp, err = i.strg.ArrivalContent().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetArrivalContents->ArrivalContent->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ArrivalContentService) Update(ctx context.Context, req *stock_service.UpdateArrivalContent) (resp *stock_service.ArrivalContent, err error) {

	i.log.Info("---UpdateArrivalContent------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ArrivalContent().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateArrivalContent--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ArrivalContent().GetByPKey(ctx, &stock_service.ArrivalContentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetArrivalContent->ArrivalContent->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ArrivalContentService) Delete(ctx context.Context, req *stock_service.ArrivalContentPrimaryKey) (resp *stock_service.Empty, err error) {

	i.log.Info("---DeleteArrivalContent------>", logger.Any("req", req))

	err = i.strg.ArrivalContent().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteArrivalContent->ArrivalContent->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &stock_service.Empty{}, nil
}
