package service

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"market_system/ms_go_stock_service/config"
	"market_system/ms_go_stock_service/genproto/stock_service"
	"market_system/ms_go_stock_service/grpc/client"
	"market_system/ms_go_stock_service/pkg/logger"
	"market_system/ms_go_stock_service/storage"
)

type RemainderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*stock_service.UnimplementedRemainderServiceServer
}

func NewRemainderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *RemainderService {
	return &RemainderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *RemainderService) Create(ctx context.Context, req *stock_service.CreateRemainder) (resp *stock_service.Remainder, err error) {

	i.log.Info("---CreateRemainder------>", logger.Any("req", req))

	pKey, err := i.strg.Remainder().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateRemainder->Remainder->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Remainder().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyRemainder->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RemainderService) GetByID(ctx context.Context, req *stock_service.RemainderPrimaryKey) (resp *stock_service.Remainder, err error) {

	i.log.Info("---GetRemainderByID------>", logger.Any("req", req))

	resp, err = i.strg.Remainder().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRemainderByID->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RemainderService) GetByBranchID(ctx context.Context, req *stock_service.RemainderBarcodeBranchIdKey) (resp *stock_service.Remainder, err error) {

	i.log.Info("---GetRemainderByBranchId------>", logger.Any("req", req))

	resp, err = i.strg.Remainder().GetByBranchKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRemainderByBranchId->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RemainderService) GetList(ctx context.Context, req *stock_service.GetListRemainderRequest) (resp *stock_service.GetListRemainderResponse, err error) {

	i.log.Info("---GetRemainders------>", logger.Any("req", req))

	resp, err = i.strg.Remainder().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRemainders->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RemainderService) Update(ctx context.Context, req *stock_service.UpdateRemainder) (resp *stock_service.Remainder, err error) {

	i.log.Info("---UpdateRemainder------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Remainder().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateRemainder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Remainder().GetByPKey(ctx, &stock_service.RemainderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetRemainder->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *RemainderService) Delete(ctx context.Context, req *stock_service.RemainderPrimaryKey) (resp *stock_service.Empty, err error) {

	i.log.Info("---DeleteRemainder------>", logger.Any("req", req))

	err = i.strg.Remainder().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteRemainder->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &stock_service.Empty{}, nil
}
