package grpc

import (
	"market_system/ms_go_stock_service/config"
	"market_system/ms_go_stock_service/genproto/stock_service"
	"market_system/ms_go_stock_service/grpc/client"
	"market_system/ms_go_stock_service/grpc/service"
	"market_system/ms_go_stock_service/pkg/logger"
	"market_system/ms_go_stock_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	stock_service.RegisterComingServiceServer(grpcServer, service.NewComingService(cfg, log, strg, srvc))
	stock_service.RegisterArrivalContentServiceServer(grpcServer, service.NewArrivalContentService(cfg, log, strg, srvc))
	stock_service.RegisterRemainderServiceServer(grpcServer, service.NewRemainderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
