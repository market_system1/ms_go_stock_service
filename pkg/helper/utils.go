package helper

import (
	"context"
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func NewIncrementId(db *pgxpool.Pool, incrementID, tableName string, prifix string, Length int) (func() string, error) {
	var (
		id sql.NullString
		// capitalLetter = string(strings.ToUpper(tableName)[0])
		query = fmt.Sprintf("SELECT %s FROM %s ORDER BY created_at DESC LIMIT 1", incrementID, tableName)
	)
	contex, cancelF := context.WithDeadline(context.Background(), time.Now().Add(time.Millisecond*2))

	resp := db.QueryRow(contex, query)

	resp.Scan(&id)
	defer cancelF()

	if !id.Valid {
		// fmt.Printf("$$$$$$$$$$$$$$     %+v     $$$$$$$$$$$$$$", id)
		id.String = ""
	}
	return func() string {
		idNumber := idToInt(id.String)
		fmt.Println(query)

		idNumber++
		var (
			numberLenght = idNumber
			count        = 0
		)
		for numberLenght > 0 {
			numberLenght /= 10
			count++
		}
		if count == 0 {
			count++
		}
		// fmt.Printf("COUNT %d  Length %d  &&&&&&&&&&&\n", count, idNumber)
		prifix = Prifix(prifix)

		fmt.Println("@@@@@@@@@@:>>>", prifix)
		id := fmt.Sprintf("%s-%s%d", prifix, strings.Repeat("O", Length-count), idNumber)
		fmt.Println(id, Length, count, idNumber, id)
		return id

	}, nil
}

func Prifix(str string) string {
	prifix_Up := strings.ToUpper(str)
	prifix := ""
	for _, v := range strings.Split(prifix_Up, " ") {
		prifix += string(v[0])
		fmt.Println("..............", v)
	}
	return prifix

}
func idToInt(DatabaseId string) int {
	pattern := regexp.MustCompile("[0-9]+")
	firstMatchSubstring := pattern.FindString(DatabaseId)
	id, err := strconv.Atoi(firstMatchSubstring)
	if err != nil {
		return 0
	}
	fmt.Println("#####################@@@@@@  ", id, "   ", firstMatchSubstring, "  @@@@@@####################")

	return id
}

// IfElse evaluates a condition, if true returns the first parameter otherwise the second
func IfElse(condition bool, a interface{}, b interface{}) interface{} {
	if condition {
		return a
	}
	return b
}
