

-- Приход
CREATE TYPE STATUS_COMING AS ENUM('IN_PROGRESS', 'FINISH');
CREATE TABLE IF NOT EXISTS "coming" (
    "id" UUID PRIMARY KEY,
    "coming_id" VARCHAR(20) NOT NULL,
    "branch_id" UUID NOT NULL,
    "provider_id" UUID  NOT NULL,
    "date_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "status" STATUS_COMING DEFAULT 'IN_PROGRESS',
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

-- Приход-товары
CREATE TABLE IF NOT EXISTS "arrival_content" (
    "id" UUID PRIMARY KEY,
    "coming_id" UUID REFERENCES "coming"("id") NOT NULL,
    "brand_id" UUID  NOT NULL,
    "category_id" UUID  NOT NULL,
    "name" VARCHAR UNIQUE NOT NULL,
    "barcode" VARCHAR UNIQUE NOT NULL,
    "quantity" INT NOT NULL,
    "price_arrival" DECIMAL NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

-- Остаток
CREATE TABLE IF NOT EXISTS "remainder" (
    "id" UUID PRIMARY KEY NOT NULL,
    "branch_id" UUID NOT NULL,
    "brand_id" UUID NOT NULL,
    "category_id" UUID NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "barcode" VARCHAR UNIQUE NOT NULL,
    "price_arrival" DECIMAL NOT NULL,
    "quantity" INT NOT NULL,
    "total" DECIMAL NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);